import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DropdownWithCheckboxComponent } from './module/dropdown-with-checkbox/dropdown-with-checkbox.component';

@NgModule({
  declarations: [
    AppComponent,
    DropdownWithCheckboxComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
