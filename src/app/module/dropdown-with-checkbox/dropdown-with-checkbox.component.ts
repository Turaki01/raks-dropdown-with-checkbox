import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dropdown-with-checkbox',
  templateUrl: './dropdown-with-checkbox.component.html',
  styleUrls: ['./dropdown-with-checkbox.component.scss']
})
export class DropdownWithCheckboxComponent implements OnInit {

  isDropDown: boolean;
  options = ['All location', 'Seamfix', 'iTranscript', 'BioRegistra', 'Fuel Voucher', 'iClocker', 'verified'];
  selectedOptions = [];

  constructor() { }

  ngOnInit() {
  }

  showDropDown() {
    this.isDropDown = !this.isDropDown;
  }

  selectOption(e) {
    console.log(e.target.checked);
    if (e.target.checked) {
      this.selectedOptions.push(e.target.id);
    } else {
      this.selectedOptions.pop()
    }
  }
}
