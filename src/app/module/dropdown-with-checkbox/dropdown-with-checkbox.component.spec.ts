import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DropdownWithCheckboxComponent } from './dropdown-with-checkbox.component';

describe('DropdownWithCheckboxComponent', () => {
  let component: DropdownWithCheckboxComponent;
  let fixture: ComponentFixture<DropdownWithCheckboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DropdownWithCheckboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownWithCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
